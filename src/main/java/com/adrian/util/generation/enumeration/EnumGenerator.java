/*
 * Copyright 2016 Adrian Bingener.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adrian.util.generation.enumeration;

/**
 * EnumGenerators are used to generate enumerations based on a certain input.
 * The generated enum is returned as a string that can be written into a java
 * file. In this case you have to make sure that the package name matches the
 * target folder.
 *
 * @author Adrian Bingener
 * @param <T> The type of input that is used to generate the enum.
 * @param <E> The type of the input keys that are processed to generate the enum
 * constants.
 */
public abstract class EnumGenerator<T, E> {

    /**
     * An integer constants that is used as key for the configuration to specify
     * what indent is used.
     */
    public static final int INDENT = 100;
    /**
     * The key filter that is bound to this enum generator.
     */
    protected KeyFilter<E> keyFilter;

    /**
     * The key converter that is bound to this enum generator.
     */
    protected KeyConverter<E> keyConverter;
    /**
     * Specifies options for the generator. The options that are actually used
     * may depend on the implementation. So each subclass should provide their
     * own configuration key-value pairs.
     */
    protected GeneratorConfiguration configuration;

    /**
     * Creates a new EnumGenerator with an empty configuration.
     */
    protected EnumGenerator() {
        this.configuration = new GeneratorConfiguration();
    }

    /**
     * Creates a new EnumGenerator with the given configuration.
     *
     * @param configuration The initial configuration for this generator
     */
    protected EnumGenerator(GeneratorConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Creates a new EnumGenerator with the given configuration.
     *
     * @param configuration The initial configuration for this generator
     * @param keyFilter The key filter for this generator
     * @param keyConverter The key converter for this generator
     */
    protected EnumGenerator(GeneratorConfiguration configuration, KeyFilter keyFilter, KeyConverter keyConverter) {
        this(configuration);
        this.keyFilter = keyFilter;
        this.keyConverter = keyConverter;
    }

    /**
     * Generates an enumeration as string based on the given input.
     *
     * @param input The input that is used to generate the input.
     * @param header The header of the enum. This should contain the package
     * declaration as well as the license header and all imports that are
     * required (e.g. <code>package com.name.app.constants;</code>).
     * @param declaration The enum declaration body including all interface
     * implementations (e.g.
     * <code>public enum TranslationKeys implements AnInterface {</code>).
     * @param footer The footer is placed below all enum constants and should
     * therefore contain all additional methods (e.g.
     * <code>public abstract String getDefaultValue(); }</code>).
     * @return
     */
    public abstract String generateEnum(T input, String header, String declaration, String footer);

    public KeyFilter<E> getKeyFilter() {
        return keyFilter;
    }

    public void setKeyFilter(KeyFilter<E> keyFilter) {
        this.keyFilter = keyFilter;
    }

    public KeyConverter<E> getKeyConverter() {
        return keyConverter;
    }

    public void setKeyConverter(KeyConverter<E> keyConverter) {
        this.keyConverter = keyConverter;
    }
}
