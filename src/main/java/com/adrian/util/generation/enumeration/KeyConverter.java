/*
 * Copyright 2016 Adrian Bingener.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adrian.util.generation.enumeration;

/**
 * A KeyConverter is used to convert a given key into another form before it is
 * put into the resulting enum. This step is done after filtering the keys. This
 * means, that converting the keys using a KeyConverter will not affect the
 * result of {@link IKeyFilter#accept(java.lang.Object)}.
 *
 * @author Adrian Bingener
 * @param <T> The type of input keys that are used to build the enum keys.
 */
public interface KeyConverter<T> {

    /**
     * Converts the given key into another form before it is put into the
     * resulting enum.
     *
     * @param key The key that is converted.
     * @return The converted key.
     */
    public String convertKey(T key);
}
