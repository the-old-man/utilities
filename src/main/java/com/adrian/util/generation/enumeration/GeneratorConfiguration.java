/*
 * Copyright 2016 Adrian Bingener.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adrian.util.generation.enumeration;

import java.util.HashMap;
import java.util.Map;

/**
 * This configuration class is used to provide options for a
 * {@link EnumGenerator}.
 *
 * @author Adrian Bingener
 */
public class GeneratorConfiguration {

    /**
     * The map that stores the configuration.
     */
    private final Map<Integer, String> configuration;

    /**
     * Creates a new instance of {@link GeneratorConfiguration} with an empty
     * map of configurations.
     */
    public GeneratorConfiguration() {
        configuration = new HashMap<>();
    }

    /**
     * Returns the current map of configurations.
     *
     * @return the current map of configurations
     */
    public Map<Integer, String> getConfiguration() {
        return configuration;
    }

    /**
     * Delegation method to access a certain value in the stored map.
     *
     * @param key The key that is used to access the value.
     * @return The value that is associated with the given key
     * @see Map#get(java.lang.Object)
     */
    public String get(Integer key) {
        return configuration.get(key);
    }
}
