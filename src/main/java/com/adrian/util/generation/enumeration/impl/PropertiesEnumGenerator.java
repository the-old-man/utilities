/*
 * Copyright 2016 Adrian Bingener.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adrian.util.generation.enumeration.impl;

import com.adrian.util.generation.enumeration.EnumGenerator;
import com.adrian.util.generation.enumeration.GeneratorConfiguration;
import com.adrian.util.generation.enumeration.KeyConverter;
import com.adrian.util.generation.enumeration.KeyFilter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 * A class to generate enumerations from properties files.
 *
 * @author Adrian Bingener
 */
public class PropertiesEnumGenerator extends EnumGenerator<Properties, String> {

    public PropertiesEnumGenerator() {
        super();
    }

    public PropertiesEnumGenerator(GeneratorConfiguration configuration) {
        super(configuration);
    }

    public PropertiesEnumGenerator(GeneratorConfiguration configuration, KeyFilter keyFilter, KeyConverter keyConverter) {
        super(configuration, keyFilter, keyConverter);
    }

    @Override
    public String generateEnum(Properties proeprties, String header, String declaration, String footer) {

        // Create builder and read the configuration
        StringBuilder builder = new StringBuilder();
        String indent = configuration.get(INDENT);

        // Append the header first
        builder.append(header).append("\n\n");

        // Append the declaration
        builder.append(declaration);

        /*
         * We need to iterate the key set twice, since we don't know in the
         * first run, if all future elements may be not accepted by the filter.
         * If that was the case, then the ';' had to be added. But since 
         * iterator.hasNext() only tells that more elements will come, but not
         * if they are acceppted by the filter, we need to filter the key set at
         * first and build the enum afterwards.
         */
        // Create copy since the caller may want to keep the key set
        Set<Object> keySet = new HashSet<>(proeprties.keySet());
        Iterator iterator = keySet.iterator();
        if (keyFilter != null) {
            while (iterator.hasNext()) {
                String key = String.valueOf(iterator.next());

                // Cancel current run if the object is not accepted by the filter
                if (!keyFilter.accept(key)) {
                    iterator.remove();
                }
            }
        }

        // Reset the iterator and build the enum
        iterator = keySet.iterator();
        while (iterator.hasNext()) {
            String key = String.valueOf(iterator.next());

            // Apply converter to key if present
            if (keyConverter != null) {
                key = keyConverter.convertKey(key);
            }

            builder.append("\n");
            builder.append(indent);
            builder.append(key);

            /*
             * Append a ',' if another elemnt follows. This is now possible,
             * since we know that all upcoming elements will be part of the
             * enum.
             */
            if (iterator.hasNext()) {
                builder.append(",");
            } else {
                builder.append(";");
            }
        }

        return builder.toString().concat("\n").concat(footer);
    }

    public void setConfiguration(GeneratorConfiguration configuration) {
        this.configuration = configuration;
    }
}
