/*
 * Copyright 2016 Adrian Bingener.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.adrian.util.generation.enumeration;

/**
 * A simple interface that is used to check if a given element is accepted or
 * not.
 *
 * @author Adrian Bingener
 * @param <T> The type of input keys that are filtered.
 */
public interface KeyFilter<T> {

    /**
     * Checks if the given element is accepted by the filter.
     *
     * @param object The element that is checked.
     * @return <code>true</code> if the element is accepted, or
     * <code>false</code> otherwise.
     */
    public boolean accept(T object);
}
