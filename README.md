# About
This utilities project is a collection of helper libraries. Right now it only
provides a library to generate Java enums from given properties.

# Enum Generator
The enum generator package provides classes to generate Java enums from
properties objects. This is helpful if you want to use the properties keys to
access values from a ResourceBundle from inside you source code.

## Usage
```java
Properties properties = new Properties();
properties.load(new FileInputStream("/path/to/source/messages.properties"));

EnumGenerator<Properties, String> generator = new PropertiesEnumGenerator();
String header = "package your.package;";
String declaration = "public enum EnumName {";
String footer = "}";
String string = generator.generateEnum(properties, header, declaration, footer);
// Write string into target file...
```
## Filter Keys
If you only want to use a subset of the properties keys that are used to build
the enum, you can set a KeyFilter. The example below shows a KeyFilter that only
accepts keys that do not contain a dot `.` symbol.
```java
KeyFilter enumKeyFilter = new KeyFilter() {
    @Override
    public boolean accept(Object object) {
        return !String.valueOf(object).contains(".");
    }
};
generator.setKeyFilter(enumKeyFilter);
```

## Convert Keys
You can also convert keys before they are used as enum constants. The example
below shows a KeyConverter that ensures that all letters are uppercase.
```java
KeyConverter keyConverter = new KeyConverter<String>() {
    @Override
    public String convertKey(String key) {
        return key.toUpperCase();
    }
};
generator.setKeyConverter(keyConverter);
```

## License

    Copyright 2016 Adrian Bingener

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.